#include <iostream>
#include "sqlite3.h"
#include <conio.h>
#include <vector>
#include <sstream>

using namespace std;

void addData();
vector<string> split(string words);
int callback(void* data, int argc, char** argv, char** azColName);

int main()
{
	sqlite3* DB;
	char* err;
	int exit = 0;
	char c = '-';
	vector<string> words;
	string word;
	exit = sqlite3_open("test.db", &DB);
	while (true)
	{
		c = _getch();
		system("cls");
		if (c == '\r' || c == '\t')
			break;
		words.clear();
		word += c;
		cout << word << endl;
		string sql = "SELECT * FROM WORDS WHERE WORD LIKE '" + word + "%';";
		sqlite3_exec(DB, sql.c_str(), callback, &words, &err);
		for (auto i : words)
		{
			cout << i << endl;
		}
	}
	if (words.size() > 0)
		cout << "Your chosen word is '" << words[0] << "'.\n";
	else
		cout << "You haven't chosen a word!\n";
	sqlite3_close(DB);
	return 0;
}

void addData()
{
	sqlite3* DB;
	char* err;
	int exit = 0;
	string words = "False await else import pass None break except " 
		"in raise True class finally is return and continue for lambda " 
		"try as def from nonlocal while assert del global not with async " 
		"elif if or yield abs delattr hash memoryview set all dict help " 
		"min setattr any dir hex next slice ascii divmod id object sorted " 
		"bin enumerate input oct staticmethod bool eval int open str " 
		"breakpoint exec isinstance ord sum bytearray filter issubclass " 
		"pow super bytes float iter print tuple callable format len property " 
		"type chr frozenset list range vars classmethod getattr locals repr " 
		"zip compile globals map reversed complex hasattr max round";
	vector<string> wordsList = split(words);
	exit = sqlite3_open("test.db", &DB);
	for (auto i : wordsList)
	{
		string sql = "INSERT INTO WORDS VALUES ('" + i + "');";
		sqlite3_exec(DB, sql.c_str(), nullptr, nullptr, &err);
	}

	sqlite3_close(DB);
}

vector<string> split(string words)
{
	vector<string> result;
	std::stringstream ss(words);
	std::string token;
	while (std::getline(ss, token, ' ')) {
		result.push_back(token);
	}
	return result;
}

int callback(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)
	{
		(*(vector<string>*)data).push_back(string(argv[i]));
	}
	return 0;
}
